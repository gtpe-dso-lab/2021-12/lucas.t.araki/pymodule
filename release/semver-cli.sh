#!/bin/bash
##################################################
SCRIPT=$(basename "${0}")
SCRIPTPATH=$(cd $(dirname "${0}") && pwd -P)
FULLSCRIPT="${SCRIPTPATH}/${SCRIPT}"
##################################################
##################################################
function syntax {
    printf "syntax: ${SCRIPT} command [version] [type]\n"
    printf "   command: validate|bump|latest|numeric\n"
}
##################################################
if [ -z "$1" ]; then
    syntax
    exit 1
else
    CMD=$(echo "$1" | tr '[:upper:]' '[:lower:]')
fi
if [ "$1" != "latest"   ] && [ -z "$2" ]; then
    syntax
    exit 1
else
   VERSION="$2"
fi
TYPE=$3
##################################################
function echo_error {
  echo -e "$1" >&2
}

function version-latest {
  L=`cat VERSION`
  if [ -z "${L}" ]; then
    L="0.0.0"
  fi
  if [ "$#" -eq "2" ]; then
    eval "$2=\"$L\""
  else
    echo "$L"
  fi
}

# convert semver to an integer for sorting
# this function supports major minor patch and rc values up to 999 each
# example calculation: echo $(( 888 * 10**9 + 777 * 10**6 + 666 * 10**3 + 5))
function version-numeric {
  local V=$1; local vdata; local RV;

  version-validate "${V}"
  if [ $? -ne 0 ]; then
    return 1
  fi
  version-parse "${V}" vdata
  if [ $? -ne 0 ]; then
    return 2
  fi
  local IM=$(( ${vdata[0]} * 10**9 ))
  local IN=$(( ${vdata[1]} * 10**6 ))
  local IP=$(( ${vdata[2]} * 10**3))
  local RC=0
  if [ ! -z ${vdata[3]} ]; then
    rcstr=${vdata[3]}
    RC=${rcstr//-rc\./}
  fi
  local RV=$(( $IM + $IN + $IP + $RC ))
  echo "$RV"
  return 0
}

function version-validate {
  local version=$1
  local SEMVER_REGEX="^[vV]?(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)(\\-rc\\.(0|[1-9][0-9]*))?$"
  if [[ "$version" =~ $SEMVER_REGEX ]]; then
    return 0
  fi
  return 1
}

function version-parse {
  local version=$1
  local SEMVER_REGEX="^[vV]?(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)(\\-rc\\.(0|[1-9][0-9]*))?$"
  if [[ "$version" =~ $SEMVER_REGEX ]]; then
    # if a second argument is passed, store the result in array var named by $2
    if [ "$#" -eq "2" ]; then
      local major=${BASH_REMATCH[1]}
      local minor=${BASH_REMATCH[2]}
      local patch=${BASH_REMATCH[3]}
      local rc=${BASH_REMATCH[4]}
      # set second argument to an array value
      eval "$2=(\"$major\" \"$minor\" \"$patch\" \"$rc\")"
    else
      echo "$version"
    fi
    return 0
  else
    echo_error "version $version does not match the semver scheme 'X.Y.Z(-rc.R)'."
  fi
  return 1
}

function version-bump {
  local VALID_TYPES='major,minor,patch,rc'
  local new; local version=$1; local rtype=$2;

  version-parse "$version" parts
  if [ $? -ne 0 ]; then
    return 1
  fi
  local major="${parts[0]}"
  local minor="${parts[1]}"
  local patch="${parts[2]}"
  local rcstr="${parts[3]}"
  # strip rc out of rcstr using bash regex search replace
  rc=${rcstr//-rc\./}
  # if rc undef then set to 0
  test -z $rc && rc=0

  case "$rtype" in
    major) new="$((major + 1)).0.0";;
    minor) new="${major}.$((minor + 1)).0";;
    patch) new="${major}.${minor}.$((patch + 1))";;
    rc) new="${major}.${minor}.${patch}-rc.$((rc + 1))";;
    *) 
      echo_error "Invalid release type: $rtype. Valid options are $VALID_TYPES"; 
      return -1
      ;;
  esac
  if [ "$#" -eq "3" ]; then
    eval "$3=\"$new\""
  else
    echo "$new"
  fi
  return 0
}
##################################################
case "$CMD" in
    validate) 
      version-validate "${VERSION}"
      if [ $? -ne 0 ]; then
        echo_error "version $VERSION does not match the semver scheme 'X.Y.Z(-rc.R)'."
        exit 1
      fi
      ;;
    latest) 
      L=$(version-latest)
      if [ $? -ne 0 ]; then
        exit 1
      else
        printf "%s\n" "${L}"
      fi
      ;;
    numeric) 
      V=$(version-numeric "${VERSION}")
      if [ $? -ne 0 ]; then
        exit 1
      else
        printf "%s %s\n" "${V} ${VERSION}"
      fi
      ;;
    bump) 
      version-bump "$VERSION" "$TYPE" NEW_VERSION
      if [ $? -ne 0 ]; then
        exit 1
      else
        printf "%s\n" "${NEW_VERSION}"
      fi
      ;;
    *) echo_error "Invalid command."; syntax; exit 1;;
esac
